<!-- 
    Muhammad Aqila Karindra Daffa
    aqiladaffa92@gmail.com
-->
<?php
class Animal{
  // Properties
  public $name;
  public $legs;
  public $cold_blooded;

  function __construct($name) {
    $this->name = $name;  
  }

  // Methods
  public function set_name($name) {
    $this->name = $name;
  }
  public function get_name() {
    return $this->name;
  }
  public function get_legs() {
    return $this->legs = 4;
  }
  public function get_cold_blooded() {
    return $this->cold_blooded = "no";
  }
}

?>
