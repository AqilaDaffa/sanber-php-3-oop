<!-- 
    Muhammad Aqila Karindra Daffa
    aqiladaffa92@gmail.com
-->
<?php 
require_once 'animal.php';
class Frog extends Animal {
    public function jump() {
      return "Hop Hop"; 
    }
  }
?>