<!-- 
    Muhammad Aqila Karindra Daffa
    aqiladaffa92@gmail.com
-->
<?php 
    require_once 'animal.php';
    require_once 'Frog.php';
    require_once 'Ape.php';
    $sheep = new Animal("shaun");

    echo "Name: $sheep->name";
    echo "<br>";
    echo "Legs: ".$sheep->get_legs(); 
    echo "<br>";
    echo "cold blooded: ".$sheep->get_cold_blooded(); 
    echo "<br>"."<br>";

    $kodok = new Frog("buduk");
    echo "Name: $kodok->name";
    echo "<br>";
    echo "Legs: ".$kodok->get_legs(); 
    echo "<br>";
    echo "cold blooded: ".$kodok->get_cold_blooded(); 
    echo "<br>";
    echo "Jump: ".$kodok->jump(); // "hop hop"
    echo "<br>"."<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name: $sungokong->name";
    echo "<br>";
    echo "Legs: ".$sungokong->get_legs(); 
    echo "<br>";
    echo "cold blooded: ".$sungokong->get_cold_blooded();
    echo "<br>";
    echo "Yell: ".$sungokong->yell();
    echo "<br>"."<br>";
?> 