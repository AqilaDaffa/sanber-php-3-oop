<!-- 
    Muhammad Aqila Karindra Daffa
    aqiladaffa92@gmail.com
-->
<?php 
require_once 'animal.php';
class Ape extends Animal{
    public function yell() {
      return "Auooo"; 
    }
    public function get_legs() {
        return $this->legs = 2;
      }
  }
?>